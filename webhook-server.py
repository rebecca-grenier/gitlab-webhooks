#!/usr/bin/python

"""
A lightweight HTTPS server for use with GitLab webhooks

source: https://gitlab.uvm.edu/saa/gitlab-webhooks
author: Brian O'Donnell <brian.odonnell@uvm.edu>
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json, re, ssl
import subprocess
from fabric.api import *
import logging

# Customize this string before use!!!
SECRET_KEY = 'IMX9JQFC8W'

# Port to bind the http server to
LISTEN_PORT = 8000

# Path to SSL certificate
CERT_FILE = '/users/w/e/webmster/gitlab-webhooks/webhook-server.pem'

class WebhookHandler(BaseHTTPRequestHandler):
    """
    An inheritable class which implements a basic GitLab webhooks API
    """
    def exec_push(self, data):
        raise NotImplementedError('exec_push')

    def exec_tag_push(self, data):
        raise NotImplementedError('exec_tag_push')

    def exec_note(self, data):
        raise NotImplementedError('exec_note')

    def exec_issue(self, data):
        raise NotImplementedError('exec_issue')

    def exec_merge_request(self, data):
        raise NotImplementedError('exec_merge_request')

    def exec_build(self, data):
        raise NotImplementedError('exec_build')

    def respond(self, code, message):
        """
        Sends a customizable HTTP response
        """
        self.send_response(code)
        self.send_header("Content-type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

    def do_POST(self):
        """
        Handles POST requests
        """
        try:
            # slurp in the request body
            self.rfile._sock.settimeout(5)
            content = self.rfile.read(int(self.headers['Content-Length']))
            postdata = json.loads(content)

            # parse out the 'secret key' from the url path
            m = re.match('\/([A-Za-z0-9]+)\/?$', self.path)

            if m:
                # Force the user to set a custom secret key
                if SECRET_KEY == 'SUPERSECRETALPHANUMERICSTRING' or SECRET_KEY == '' or SECRET_KEY is None:
                    raise ValueError('Please set a secret key!')

                if m.group(1) != SECRET_KEY:
                    self.respond(400, 'Invalid secret key')
                elif 'object_kind' in postdata:
                    action = postdata['object_kind']

                    if action == 'push':
                        self.exec_push(postdata)
                    elif action == 'tag_push':
                        self.exec_tag_push(postdata)
                    elif action == 'issue':
                        self.exec_issue(postdata)
                    elif action == 'note':
                        self.exec_note(postdata)
                    elif action == 'merge_request':
                        self.exec_merge_request(postdata)
                    elif action == 'build':
                        self.exec_build(postdata)
                    else:
                        self.respond(400, 'Invalid object_kind attribute')
                        return

                    self.respond(200, 'OK')
                else:
                    self.respond(400, 'Missing object_kind attribute')
            else:
                self.respond(400, 'Missing or invalid secret key in url')
        except NotImplementedError as e:
            self.respond(501, 'Method %s not implemented' % e)
        except (RuntimeError, ValueError) as e:
            self.respond(500, '%s' % e)


def update_git_files(branch, after, alias):
    logging.info("I want to autodeploy a change to newrev: %s in environment: %s" % (after, branch))
    run('git --git-dir=/var/www/%s.drup-lb.uvm.edu/.git fetch neworigin' % alias)
    run('git --work-tree=/var/www/{alias}.drup-lb.uvm.edu/ --git-dir=/var/www/{alias}.drup-lb.uvm.edu/.git checkout neworigin/{branch}'.format(alias=alias,branch=branch))
    run('git --work-tree=/var/www/%s.drup-lb.uvm.edu/ --git-dir=/var/www/%s.drup-lb.uvm.edu/.git reset --hard' % (alias, alias))


class CustomHookHandler(WebhookHandler):
    """
    Override parent class' exec_ methods here
    """

    def update_db(self, env):
	subprocess.call('drush @%s sql-drop --yes' % env, shell=True)
	test = subprocess.call('drush sql-sync @prod @%s --yes' % env, shell=True)
        logging.info(test)	

    def exec_tag_push(self, data):
        alias = "prod"
        branch = "production"
        after = data["after"]

        local('drush @%s cc all' % alias)
        local('drush @%s vset maintenance_mode 1' % alias)
        results = execute(update_git_files, "production", after, alias, hosts=['drup-ws1','drup-ws2'])
        local('drush @%s updatedb --yes' % alias)
        local('drush @%s vset maintenance_mode 0' % alias)
        local('drush @%s cc all' % alias)
        logging.info("Sucessfully finished deployment to Production");

    def exec_push(self, data):

        branch = data["ref"].replace("refs/heads/","")
        after = data["after"]

        if branch in ["staging", "qa"]:
          local('drush @prod cc all')
          self.update_db(branch)
          local('drush @%s vset maintenance_mode 1' % branch)
          results = execute(update_git_files, branch, after, branch, hosts=['localhost'])
          local('drush @%s updatedb --yes' % branch)
          local('drush @%s vset maintenance_mode 0' % branch)
          local('drush @%s cc all' % alias)
          subprocess.call('rsync -r --progress --delete webmster@drup-ws1.uvm.edu:/var/www/prod.drup-lb.uvm.edu/sites/default/files /var/www/%s.drup-lb.uvm.edu/sites/default/' % branch, shell=True)
          logging.info("Sucessfully Finished deployment to %s" % branch);
    pass


def main():
    """
    Starts the webserver
    """
    try:
    	logging.basicConfig(level=logging.INFO, filename="drupal-deploy.log", filemode="a+", 
	     format="%(asctime)-15s %(levelname)-8s %(message)s")
    	server = HTTPServer(('', LISTEN_PORT), CustomHookHandler)
        server.socket = ssl.wrap_socket(server.socket, certfile=CERT_FILE, server_side=True)
        server.serve_forever()
    except KeyboardInterrupt:
        logging.info('Ctrl-C pressed, shutting down.')
    except:
	e = sys.exc_info()[0]
   	logging.info( "Error: %s" % e )
    finally:
        server.socket.close()

if __name__ == '__main__':
    main()
