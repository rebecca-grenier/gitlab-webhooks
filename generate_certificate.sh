#!/bin/bash

/usr/bin/openssl genrsa -des3 -out webhook-server.key 2048
/usr/bin/openssl req -new -key webhook-server.key -out webhook-server.csr
/usr/bin/openssl rsa -in webhook-server.key -out webhook-server.key
/usr/bin/openssl x509 -req -days 1825 -in webhook-server.csr -signkey webhook-server.key -out webhook-server.crt
/usr/bin/cat webhook-server.crt webhook-server.key > webhook-server.pem
/bin/rm webhook-server.key webhook-server.csr webhook-server.crt
/usr/bin/chmod 400 webhook-server.pem
